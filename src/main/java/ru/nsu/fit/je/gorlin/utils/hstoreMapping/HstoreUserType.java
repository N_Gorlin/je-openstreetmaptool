package ru.nsu.fit.je.gorlin.utils.hstoreMapping;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

public class HstoreUserType implements UserType {
    public Object assemble(Serializable cached, Object owner)
            throws HibernateException {
        return cached;
    }

    public Object deepCopy(Object o) throws HibernateException {
        Map m = (Map) o;
        return new HashMap(m);
    }

    public Serializable disassemble(Object o) throws HibernateException {
        return (Serializable) o;
    }

    public boolean equals(Object o1, Object o2) throws HibernateException {
        Map m1 = (Map) o1;
        Map m2 = (Map) o2;
        return m1.equals(m2);
    }

    public int hashCode(Object o) throws HibernateException {
        return o.hashCode();
    }

    public boolean isMutable() {
        return true;
    }

    public Object nullSafeGet(ResultSet rs, String[] strings, SharedSessionContractImplementor implementor, Object o)
            throws HibernateException, SQLException {
        String col = strings[0];
        String val = rs.getString(col);
        return HstoreHelper.toMap(val);
    }

    public void nullSafeSet(PreparedStatement ps, Object obj, int i, SharedSessionContractImplementor implementor)
            throws HibernateException, SQLException {
        String s = HstoreHelper.toString((Map) obj);
        ps.setObject(i, s, Types.OTHER);
    }

    public Object replace(Object original, Object target, Object owner)
            throws HibernateException {
        return original;
    }

    public Class returnedClass() {
        return Map.class;
    }

    public int[] sqlTypes() {
        return new int[] { Types.INTEGER };
    }
}
