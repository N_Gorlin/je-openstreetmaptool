package ru.nsu.fit.je.gorlin.models;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Table(name = "Nodes")
@Entity
public class Node extends OSMObject {
    @Column
    private double latitude;

    @Column
    private double longtitude;
}
