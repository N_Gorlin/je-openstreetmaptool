package ru.nsu.fit.je.gorlin.models;

import lombok.Data;

import java.util.ArrayList;

@Data
public class Relation extends OSMObject {
    private ArrayList<Member> members;

    @Data
    public static class Member {
        private String type;

        private int ref;

        private String role;
    }
}
