package ru.nsu.fit.je.gorlin.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.fit.je.gorlin.models.Node;

import javax.persistence.EntityManager;
import java.util.List;

@RestController
public class NearestNodesController {
    private EntityManager entityManager;

    @Autowired
    public NearestNodesController(EntityManager manager) {
        this.entityManager = manager;
    }

    @GetMapping("/nearest")
    public ResponseEntity getNearest(
            @RequestParam(name = "radius") double radius,
            @RequestParam(name = "latitude") double latitude,
            @RequestParam(name = "longitude") double longitude,
            @RequestParam(name = "limit") int limit) {
        if (limit == 0) {
            limit = 1000;
        }

        var queryCode = String.format("SELECT * " +
                "FROM osmobject " +
                "WHERE earth_distance(ll_to_earth(%f, %f), ll_to_earth(latitude, longtitude)) < %f" +
                "LIMIT %d", latitude, longitude, radius, limit);

        var query = entityManager.createNativeQuery(queryCode, Node.class);
        List<Node> nodes = query.getResultList();


        return ResponseEntity
                .status(HttpStatus.OK)
                .body(nodes);
    }
}
