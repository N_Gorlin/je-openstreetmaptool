package ru.nsu.fit.je.gorlin.utils;

import ru.nsu.fit.je.gorlin.models.Node;
import ru.nsu.fit.je.gorlin.models.OSMObject;
import ru.nsu.fit.je.gorlin.models.Relation;
import ru.nsu.fit.je.gorlin.models.Way;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OSMParser {
    private XMLStreamReader reader;

    public OSMParser(String relativePath) throws IOException, XMLStreamException {
        try {
            var is = Files.newInputStream(Paths.get(relativePath));

            var factory = XMLInputFactory.newInstance();
            this.reader = factory.createXMLStreamReader(is);
        } catch (IOException | XMLStreamException ioe) {
            throw ioe;
        }
    }

    public List<Node> readNodes(int count) throws XMLStreamException {
        var nodes = new ArrayList<Node>();

        for (int i = 0; i < count; i++) {
            var node = this.readNextNode();
            if (node == null) {
                break;
            }

            nodes.add(node);
        }

        return nodes;
    }

    private void parseOSM() {
        try {
            while (reader.hasNext()) {
                var obj = this.readNextObject(reader);
                if (obj != null) {
                    System.err.println("Readed!");
                }
            }
        } catch (XMLStreamException xse) {
            System.err.println(xse.getMessage());
        }
    }

    private Node readNextNode() throws XMLStreamException {
        while (reader.hasNext()) {
            reader.next();

            if (reader.getEventType() == XMLEvent.START_ELEMENT) {
                var fieldName = reader.getLocalName();
                if (!"node".equals(fieldName)) {
                    continue;
                }

                return this.readNode(reader);
            }
        }

        return null;
    }

    private OSMObject readNextObject(XMLStreamReader reader) throws XMLStreamException {
        reader.next();

        if (reader.getEventType() == XMLEvent.START_ELEMENT) {
            var fieldName = reader.getLocalName();

            switch (fieldName) {
                case "node":
                    var node = this.readNode(reader);
                    System.err.println(node.getTags().size());
                    return node;

                case "way":
                    var way = this.readWay(reader);
                    System.err.println(way.getTags().size());
                    System.err.println(way.getNodes().size());
                    return way;

                case "relation":
                    var relation = this.readRelation(reader);
                    System.err.println(relation.getTags().size());
                    System.err.println(relation.getMembers().size());
                    return relation;
            }
        }

        return null;
    }

    private Node readNode(XMLStreamReader reader) throws XMLStreamException {
        var node = new Node();

        var attributesNum = reader.getAttributeCount();
        for (int i = 0; i < attributesNum; i++) {
            var attributeName = reader.getAttributeLocalName(i);
            var attributeValue = reader.getAttributeValue(i);

            if (node.setBaseAttribute(attributeValue, attributeName)) {
                continue;
            }

            switch(attributeName) {
                case "lat":
                    node.setLatitude(Double.parseDouble(attributeValue));
                    break;
                case "lon":
                    node.setLongtitude(Double.parseDouble(attributeValue));
                    break;
            }
        }

        var tags = this.readTags(reader, "node");
        node.setTags(tags);

        return node;
    }

    private Way readWay(XMLStreamReader reader) throws XMLStreamException {
        var way = new Way();

        var attributesNum = reader.getAttributeCount();
        for (int i = 0; i < attributesNum; i++) {
            var attributeName = reader.getAttributeLocalName(i);
            var attributeValue = reader.getAttributeValue(i);

            way.setBaseAttribute(attributeValue, attributeName);
        }

        var nodes = new ArrayList<Integer>();

        while (true) {
            reader.next();

            if (reader.isStartElement() && "nd".equals(reader.getLocalName())) {
                var id = Integer.parseInt(reader.getAttributeValue(0));
                nodes.add(id);
            }

            if ((reader.isEndElement() && "way".equals(reader.getLocalName()))
                    || (reader.isStartElement() && "tag".equals(reader.getLocalName()))) {
                break;
            }
        }

        way.setNodes(nodes);

        var tags = this.readTags(reader, "way");
        way.setTags(tags);

        return way;
    }

    private Relation readRelation(XMLStreamReader reader) throws XMLStreamException {
        var relation = new Relation();

        var attributesNum = reader.getAttributeCount();
        for (int i = 0; i < attributesNum; i++) {
            var attributeName = reader.getAttributeLocalName(i);
            var attributeValue = reader.getAttributeValue(i);

            relation.setBaseAttribute(attributeValue, attributeName);
        }

        var members = new ArrayList<Relation.Member>();

        while (true) {
            reader.next();

            if (reader.isStartElement() && "member".equals(reader.getLocalName())) {
                var member = this.readMember(reader);
                members.add(member);
            }

            if ((reader.isEndElement() && "relation ".equals(reader.getLocalName()))
                    || (reader.isStartElement() && "tag".equals(reader.getLocalName()))) {
                break;
            }
        }

        relation.setMembers(members);

        var tags = this.readTags(reader, "relation");
        relation.setTags(tags);

        return relation;
    }

    private HashMap<String, String> readTags(XMLStreamReader reader, String mainElement) throws XMLStreamException {
        var tags = new HashMap<String, String>();

        while (!reader.isEndElement() || !mainElement.equals(reader.getLocalName())) {
            if (reader.isStartElement() && "tag".equals(reader.getLocalName())) {
                var key = reader.getAttributeValue(0);
                var value = reader.getAttributeValue(1);

                tags.put(key, value);

            }

            reader.next();
        }

        return tags;
    }

    private Relation.Member readMember(XMLStreamReader reader) {
        var member = new Relation.Member();

        var attributesNum = reader.getAttributeCount();
        for (int i = 0; i < attributesNum; i++) {
            var attributeName = reader.getAttributeLocalName(i);
            var attributeValue = reader.getAttributeValue(i);

            switch (attributeName) {
                case "type":
                    member.setType(attributeValue);
                    break;
                case "ref":
                    member.setRef(Integer.parseInt(attributeValue));
                    break;
                case "role":
                    member.setRole(attributeValue);
                    break;
            }
        }

        return member;
    }
}
