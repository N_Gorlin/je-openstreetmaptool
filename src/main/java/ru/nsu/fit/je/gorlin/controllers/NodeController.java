package ru.nsu.fit.je.gorlin.controllers;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.nsu.fit.je.gorlin.models.Node;
import ru.nsu.fit.je.gorlin.repositories.NodeRepository;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
public class NodeController {
    private NodeRepository repository;

    @Autowired
    public NodeController(NodeRepository nodeRepository) {
        this.repository = nodeRepository;
    }

    @GetMapping("/nodes")
    public List<Node> getAllNodes() {
        return Lists.newArrayList(this.repository.findAll());
    }

    @GetMapping("/nodes/{id}")
    public ResponseEntity<Node> getNode(@PathVariable long id) {
        Optional<Node> node = this.repository.findById(id);

        return node.map(node1 -> ResponseEntity
                .status(HttpStatus.OK)
                .body(node1)).orElseGet(() -> ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(null));
    }

    @PostMapping("/nodes")
    public ResponseEntity<Object> createNode(@RequestBody Node node) {
        Node savedNode = this.repository.save(node);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedNode.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @DeleteMapping("/nodes")
    public ResponseEntity deleteAllNodes() {
        this.repository.deleteAll();

        return ResponseEntity
                .status(HttpStatus.OK)
                .body("All nodes has been deleted");
    }

    @DeleteMapping("/nodes/{id")
    public ResponseEntity<Node> deleteNode(@PathVariable long id) {
        Optional<Node> node = this.repository.findById(id);

        if (node.isPresent()) {
            this.repository.deleteById(id);

            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(node.get());
        }

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(null);

    }
}
