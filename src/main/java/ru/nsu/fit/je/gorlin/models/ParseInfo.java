package ru.nsu.fit.je.gorlin.models;

import lombok.Data;

@Data
public class ParseInfo {
    private double totalTime;
    private double totalParseTime;

    private double totalObjects;

    public ParseInfo(double totalTime, double totalParseTime, double totalObjects) {
        this.totalTime = totalTime;
        this.totalParseTime = totalParseTime;
        this.totalObjects = totalObjects;
    }
}
