package ru.nsu.fit.je.gorlin.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.fit.je.gorlin.models.ParseInfo;
import ru.nsu.fit.je.gorlin.repositories.NodeRepository;
import ru.nsu.fit.je.gorlin.utils.OSMParser;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;

@RestController
public class OSMDataController {
    private final static int DEFAULT_BATCH_SIZE = 20000;
    private NodeRepository nodeRepository;

    @Autowired
    public OSMDataController(NodeRepository nodeRepository) {
        this.nodeRepository = nodeRepository;
    }

    @PostMapping("/parse")
    public ResponseEntity parseOSM(@RequestParam(name = "path") String path) {
        ParseInfo info;
        try {
            var parser = new OSMParser(path);

            var startTime = System.currentTimeMillis();
            long size = 0;
            while (true) {
                var nodes = parser.readNodes(DEFAULT_BATCH_SIZE);
                nodeRepository.saveAll(nodes);

                if (nodes.size() < DEFAULT_BATCH_SIZE) {
                    break;
                }

                size += nodes.size();
            }

            var elapsedTime = (System.currentTimeMillis() - startTime) / 1000.0;

            info = new ParseInfo(elapsedTime, 0, size);
        }
        catch (IOException ioe) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Bad path");
        }
        catch (XMLStreamException xse) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Bad xml file format");
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(info);
    }
}
