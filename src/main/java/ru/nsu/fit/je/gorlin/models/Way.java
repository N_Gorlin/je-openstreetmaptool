package ru.nsu.fit.je.gorlin.models;

import lombok.Data;

import java.util.ArrayList;

@Data
public class Way extends OSMObject {
    private ArrayList<Integer> nodes;
}
