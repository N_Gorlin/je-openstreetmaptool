package ru.nsu.fit.je.gorlin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.atomic.AtomicInteger;

@SpringBootApplication
public class App 
{
    public static void main( String[] args )
    {
        SpringApplication.run(App.class, args);
        /*try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException cnfe) {
            System.err.println("Not found postgree driver");
            return;
        }

        try {
            Connection connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/postgres",
                    "postgres",
                    "postgres"
            );

            connection.setAutoCommit(false);

            AtomicInteger realAmount = new AtomicInteger(1000);

            Thread first = new Thread(() -> {
                try (var statement = connection.createStatement()) {
                    var query = "update account set amount = amount + 1 where id = 1";

                    for (int i = 0; i < 1000; i++) {
                        statement.executeUpdate(query);
                        connection.commit();
                        realAmount.incrementAndGet();
                    }
                    System.out.println("Here!");
                } catch (SQLException exc) {
                    exc.printStackTrace();
                }
            });

            Thread second = new Thread(() -> {
                try (var statement = connection.createStatement()) {
                    var query = "update account set amount = amount - 1 where id = 1";
                    for (int i = 0; i < 1000; i++) {
                        statement.executeUpdate(query);
                        connection.commit();
                        realAmount.decrementAndGet();
                    }
                } catch (SQLException exc) {
                    exc.printStackTrace();
                }
            });

            first.start();
            second.start();
            first.join();
            second.join();
            System.out.println(realAmount);
        } catch (Exception se) {
            System.err.println("Exception");
            se.printStackTrace();
            return;
        }*/
    }
}
