package ru.nsu.fit.je.gorlin.utils.hstoreMapping;

import java.util.HashMap;
import java.util.Map;

class HstoreHelper {
    private static final String K_V_SEPARATOR = "=>";

    static String toString(Map<String, String> m) {
        if (m.isEmpty()) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        int n = m.size();

        for (String key : m.keySet()) {
            sb.append("\"")
                    .append(key)
                    .append("\"")
                    .append(K_V_SEPARATOR)
                    .append("\"")
                    .append(m.get(key))
                    .append("\"");

            if (n > 1) {
                sb.append(", ");
                n--;
            }
        }

        return sb.toString();
    }

    static Map<String, String> toMap(String s) {
        var m = new HashMap<String, String>();

        if (s.isEmpty()) {
            return m;
        }

        String[] tokens = s.split(", ");
        for (String token : tokens) {
            String[] kv = token.split(K_V_SEPARATOR);
            String k = kv[0];
            k = k.trim().substring(1, k.length() - 1);
            String v = kv[1];
            v = v.trim().substring(1, v.length() - 1);
            m.put(k, v);
        }
        return m;
    }
}
