package ru.nsu.fit.je.gorlin.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.nsu.fit.je.gorlin.models.Node;

public interface NodeRepository extends CrudRepository<Node, Long> { }
